'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models/');
var factura = models.factura;
var persona = models.persona;
var auto = models.auto;
var detallefactura = models.detallefactura;

class FacturaController {
    async listar(req, res) {
        var lista = await factura.findAll({
            include: [
                { model: models.detallefactura, as: "detallefactura", attributes: ['cantidad', 'subtotal'] },
                //{ model: models.auto, as: "auto", attributes: ['modelo', 'placa'] },
            ],
            attributes: ['numero', 'fecha', 'metodo_pago', 'estado', 'total']
        });
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async numFacturas(req, res) {
        var num = await factura.count();
        res.json({ msg: "OK!", code: 200, facturas: num });
    }

    async guardar(req, res) {
        const facturas = JSON.parse(req.body.factura);
        
         const dataDesArray = [];
         if (Array.isArray(req.body.dataDes)) {
           for (const item of req.body.dataDes) {
             dataDesArray.push(JSON.parse(item));
           }
         } else {
           dataDesArray.push(JSON.parse(req.body.dataDes));
         }

        let persona_id = facturas.external_persona;

        if (persona_id != undefined) {

            let personaAux = await persona.findOne({ where: { external_id: persona_id } });

            if (personaAux) {
                var data = {
                    numero: facturas.numero,
                    metodo_pago: facturas.metodo,
                    estado: facturas.estado,
                    total: facturas.total,
                    id_persona: personaAux.id,
                };

                let transaction = await models.sequelize.transaction();

                try {

                    const auxFactura = await factura.create(data, { transaction });

                    for (const car of dataDesArray) { // Utilizar 'dataDes' en lugar de 'dataDesArray'
                        console.log("EXTERNAL ID CAR", car.external_id)
                        let autoAux = await auto.findOne({ where: { external_id: car.external_id } });

                        autoAux.copia_identificacion = personaAux.identificacion;
                        autoAux.estado_vendido = true;

                        var result = await autoAux.save();

                        if (result === null) {
                            res.status(400);
                            res.json({ msg: "error al agregar auto", code: 200 });
                        } else {
                            res.status(200);
                            let datos = {
                                cantidad: facturas.cantidad,
                                subtotal: facturas.subtotal,
                                id_factura: auxFactura.id,
                                id_auto: autoAux.id,
                            };
                            await detallefactura.create(datos, { transaction });
                        }

                    }

                    await transaction.commit();
                    res.json({ msg: "Se han registrado su Factura", code: 200 });
                } catch (error) {
                    if (transaction) await transaction.rollback();
                    if (error.errors && error.errors[0].message) {
                        res.json({ msg: error.errors[0].message, code: 200 });
                    } else {
                        res.json({ msg: error.message, code: 200 });
                    }
                }
            } else {
                res.status(400);
                res.json({ msg: "Datos no encontrados", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos no encontrados", code: 400 });
        }
    }


}

module.exports = FacturaController;