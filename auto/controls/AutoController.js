'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models/');
var auto = models.auto;
var marca = models.marca;
const fotoAuto = models.fotoAuto;


class AutoController {

    async listar(req, res) {
        var lista = await auto.findAll({
            where: { estado_vendido: false },
            include: [
                { model: models.marca, as: "marca", attributes: ['nombre'] },
                { model: models.fotoAuto, as: "fotoAuto", attributes: ['url'] }
              ],
            attributes: ['modelo', 'color', 'anio', 'precio', 'estado_vendido', 'external_id', 'copia_identificacion', 'placa']
        });
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async obtenerVendidos(req, res) {
        var lista = await auto.findAll({
            where: { estado_vendido: true },
            include: { model: models.marca, as: "marca", attributes: ['nombre'] },
            attributes: ['modelo', 'color', 'anio', 'precio', 'estado_vendido', 'external_id', 'copia_identificacion', 'placa']
        });
        if (lista === null) {
            lista = {};
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async obtenerNoVendidos(req, res) {
        var num = await auto.count({
            where: { estado_vendido: false },
            include: { model: models.marca, as: "marca", attributes: ['nombre'] },
        });
        res.status(200);
        res.json({ msg: "OK!", code: 200, nro: num });
    }


    async guardar(req, res) {
        const carData = JSON.parse(req.body.car);
        let marca_id = carData.external_marca;
    
        if (marca_id != undefined) {
            try {
                let marcaAux = await marca.findOne({ where: { external_id: marca_id } });
    
                if (marcaAux) {
                    let data = {
                        placa: carData.placa,
                        modelo: carData.modelo,
                        color: carData.color,
                        anio: carData.anio,
                        precio: carData.precio,
                        id_marca: marcaAux.id,
                    };
    
                    let transaction = await models.sequelize.transaction();
    
                    try {
                        const aux = await auto.create(data, { transaction });
                        
                        for (const file of req.files) {
                            const imageUrl = `http://localhost:3006/imagenes/${file.filename}`;
                            let datos = {
                                url: imageUrl,
                                id_auto: aux.id,
                            };
    
                            await fotoAuto.create(datos, { transaction });
                        }
    
                        await transaction.commit();
                        res.status(200).json({ msg: "Se han registrado sus datos", code: 200 });
                    } catch (error) {
                        await transaction.rollback();
    
                        if (error.errors && error.errors[0].message) {
                            res.status(200).json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.status(200).json({ msg: error.message, code: 200 });
                        }
                    }
                } else {
                    res.status(400).json({ msg: "Datos no encontrados", code: 400 });
                }
            } catch (error) {
                res.status(500).json({ msg: "Error interno del servidor", code: 500 });
            }
        } else {
            res.status(400).json({ msg: "Datos no encontrados", code: 400 });
        }
    }
}

module.exports = AutoController;



