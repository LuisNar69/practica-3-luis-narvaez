'use strict';

module.exports = (sequelize, DataTypes) => {
    const fotoAuto = sequelize.define('fotoAuto', {
        url: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    }, { freezeTableName: true });

    fotoAuto.associate = function (models) {
        fotoAuto.belongsTo(models.auto, { foreignKey: 'id_auto' });
    }
    return fotoAuto;
};        