import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useNavigate, Link } from 'react-router-dom';
import { obtenerPersonas, guardarFactura, Facturas } from '../hooks/Conexion';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import Header from "./Header";
import { useForm } from 'react-hook-form';
import Footer from "./Footer";
import { Autos } from '../hooks/Conexion';
import DataTable from 'react-data-table-component';


const Factura = () => {
    const navegation = useNavigate();
    const [llmarcas, setLlmarcas] = useState(false);
    const [clientes, setClientes] = useState([]);
    const [cedula, setCedula] = useState("");
    const fechaActual = new Date();
    const fechaFormateada = fechaActual.toISOString().split('T')[0];
    const [nro, setNro] = useState(0);
    const [data, setData] = useState([]);
    const [llautos, setLlautos] = useState(false);
    const [mostrarTabla, setMostrarTabla] = useState(false);
    const [selectedItems, setSelectedItems] = useState([]);
    const [dataDes, setDataDes] = useState([]);
    const { register, handleSubmit, getValues, setValue, formState: { errors } } = useForm();

    const handleSelectItem = (item) => {
        if (selectedItems.includes(item)) {
            setSelectedItems(selectedItems.filter((selectedItem) => selectedItem !== item));
        } else {
            setSelectedItems([...selectedItems, item]);
        }
    };

    const handleAgregarAuto = () => {
        setSelectedItems([])
        setMostrarTabla(true);
    };

    const handleCerrarTabla = () => {
        setMostrarTabla(false);
        // setSelectedItems([]);
    };

    const descripcionAux = () => {
        const newDataDes = selectedItems.map((item) => ({ ...item }));
        setDataDes(newDataDes);
    };

    const onSubmit = (data) => {

        if (data.metodo !== "CANCELADA") {
            const numero = "000000"+nro.toString();
            const valuesubtotal = subtotal();
            const valuetotal = total();
            const valuecantidad = selectedItems.length;

            var datos = {
                'external_persona': data.external_persona,
                'numero': numero,
                'metodo': data.metodo,
                'estado': data.estado,
                'total': valuetotal,
                'cantidad': valuecantidad,
                'subtotal': valuesubtotal,
            };
            guardarFactura(datos, getToken(), dataDes).then((info) => {
                if (info.code !== 200) {
                    if (info.msg === 'token expirado o no valido') {
                        borrarSesion();
                        mensajes(info.msg);
                        navegation("/sesion");
                    } else {
                        mensajes(info.msg, 'error', 'Error');
                    }
                } else {
                    mensajes(info.msg, 'success', 'Success');
                    navegation('/inicio');

                }
            });

        } else {
            mensajes("Factura cancelada");
        }

    };

    if (!llautos) {
        Autos(getToken()).then((info) => {
            if (info.code !== 200 && info.msg === 'token expirado o no valido') {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion");
            } else {
                setData(info.info);
                setLlautos(true);
            }
        });
    }

    if (!llmarcas) {
        obtenerPersonas(getToken()).then((info) => {
            if (info.code !== 200 && info.msg == 'token expirado o no valido') {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion");
            } else {
                setClientes(info.info);
                setLlmarcas(true);
            }
        });
    };

    const columns = [
        {
            name: 'Modelo',
            selector: row => row.modelo,
        },
        {
            name: 'Color',
            selector: row => row.color,
        },
        {
            name: 'Precio',
            selector: row => '$ ' + row.precio,
        },
        {
            name: 'Marca',
            selector: row => row.marca.nombre,
        },
    ];

    Facturas(getToken()).then((info) => {
        if (info.code !== 200 && info.msg === 'token expirado o no valido') {
            borrarSesion();
            mensajes(info.msg);
            navegation("/sesion");
        } else {
            setNro(info.facturas);
        }
    });


    const subtotal = () => {
        var subtotal = 0;

        dataDes.forEach((element) => {
            subtotal += parseFloat(element.selectedRows[0].precio);
        });
        return subtotal;
    };

    const total = () => {
        var total = 0;
        var iva = 0.12;
        iva = 0.12 * subtotal();
        total = parseFloat(iva) + subtotal();
        return total;
    };

    return (
        <div className='wrapper'>
            <Header />
            <div className="card text-center" style={{ margin: 10 }}>
                <div className="card-header" style={{ color: "black" }}>*** FACTURA ***</div>
                <div className="card-body">
                    <div className='container-fluid'>

                        <form className='user' onSubmit={handleSubmit(onSubmit)}>
                            <div className="form-group row">
                                <label className="col-lg-3 col-form-label">Número de factura:</label>
                                <div className="col-lg-3">
                                    <input {...register('numero')} type="text" className="form-control" value={`000000${nro}`} />
                                    {errors.numero && errors.numero.type === 'required' && <div className='alert alert-danger'>elija un numero</div>}
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-lg-3 col-form-label" >Fecha de emision:</label>
                                <div className="col-lg-3">
                                    <input type="text" className="form-control" disabled defaultValue={fechaFormateada} />

                                </div>
                            </div>
                            <div className="row mb-4">
                                <div className="col">
                                    <div className="form-group row">
                                        <div className="col-lg-3">
                                            <select className='form-control' style={{ height: 35, width: 400 }}  {...register('external_persona', { required: true })} onChange={(e) => setCedula(e.target.options[e.target.selectedIndex].getAttribute('ced'))}>

                                                <option>Elija un cliente</option>
                                                {clientes.map((aux, i) => {
                                                    return (<option key={i} value={aux.external_id} ced={aux.identificacion}>
                                                        {aux.nombres + " " + aux.apellidos}
                                                    </option>)

                                                })}
                                            </select>
                                            {errors.external_persona && errors.external_persona.type === 'required' && <div className='alert alert-danger'>elija un cliente</div>}
                                        </div>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label">Cedula:</label>
                                        <div className="col-lg-3">
                                            <input type="text" style={{ height: 35, width: 400 }} disabled className="form-control" id="cedulaFactura" value={cedula} />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="row mb-4">
                                <div className="col">
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label">Metodo pago:</label>
                                        <div className="col-lg-3">
                                            <select className='form-control' style={{ height: 35, width: 400 }}  {...register('metodo', { required: true })}>
                                                <option>TARJETA_DEBITO</option>
                                                <option>TARJETA_CREDITO</option>
                                                <option>EFECTIVO</option>

                                            </select>
                                            {errors.metodo && errors.metodo === 'required' && <div className='alert alert-danger'>elija un metodo</div>}
                                        </div>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label">Estado factura:</label>
                                        <div className="col-lg-3">
                                            <select className='form-control' style={{ height: 35, width: 400 }}  {...register('estado', { required: true })}>
                                                <option>PAGADA</option>
                                                <option>PENDIENTE</option>
                                                <option>CANCELADA</option>

                                            </select>
                                            {errors.estado && errors.estado === 'required' && <div className='alert alert-danger'>elija un estado de la factura</div>}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="row mt-4">
                                <div className="col-md">
                                    <button type="button" onClick={handleAgregarAuto} className="btn btn-success" style={{ margin: 10 }}>Agregar Auto</button>

                                    {mostrarTabla && (
                                        <div className="card text-center" style={{ margin: 20 }}>
                                            <div className="card-header" style={{ color: "black" }}>   AUTOS DISPONIBLES    </div>
                                            <div className="card-body">
                                                <div className='container-fluid'>

                                                    <div className="row mb-4">
                                                        <div className="col">
                                                        </div>
                                                    </div>

                                                    <DataTable columns={columns} data={data} selectableRows onSelectedRowsChange={handleSelectItem} />
                                                </div>
                                            </div>
                                            <button className='btn btn' onClick={() => { handleCerrarTabla(); descripcionAux(); }}>Agregar</button>
                                        </div>
                                    )}

                                    <table className="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Descripción</th>
                                                <th className="text-right">Precio Unitario</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            {dataDes.map((element, index) => {

                                                return (
                                                    <tr key={index}>
                                                        <td>{"auto: " + element.selectedRows[0].modelo + ", color: " + element.selectedRows[0].color + ", placa: " + element.selectedRows[0].placa}</td>
                                                        <td>{element.selectedRows[0].precio}</td>
                                                    </tr>
                                                );
                                            })}
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <div className="row mb-4">
                                <div className="col">
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label">Total:</label>
                                        <div className="col-lg-3">
                                            <input type="text" {...register('total')} style={{ height: 35, width: 400 }} className="form-control" value={total()} />
                                            {errors.total && errors.total.type === 'required' && <div className='alert alert-danger'>elija un cliente</div>}
                                        </div>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label">Subtotal:</label>
                                        <div className="col-lg-3">
                                            <input type="text" {...register('subtotal')} style={{ height: 35, width: 400 }} className="form-control" value={subtotal()} />
                                            {errors.subtotal && errors.subtotal.type === 'required' && <div className='alert alert-danger'>elija un cliente</div>}
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className="col">
                                <div className="form-group row">
                                    <label className="col-lg-3 col-form-label">Cantidad:</label>
                                    <div className="col-lg-3">
                                        <input {...register('cantidad')} type="text" style={{ height: 35, width: 400 }} className="form-control" value={selectedItems.length} />

                                    </div>
                                </div>
                            </div>
                            <button type='submit' className="btn btn-success">Guardar Factura</button>
                        </form>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
};

export default Factura;