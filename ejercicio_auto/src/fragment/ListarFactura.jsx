import React, { useState } from 'react';
import { Autos, ListarFacturas } from '../hooks/Conexion';
import { useNavigate } from 'react-router-dom';
import Header from './Header';
import Footer from "./Footer";
import DataTable from 'react-data-table-component';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';

const ListarFactura = () => {
    const navegation = useNavigate();
    const [data, setData] = useState([]);
    const [llfactura, setFlautos] = useState(false);


    if (!llfactura) {
        ListarFacturas(getToken()).then((info) => {
            if (info.code !== 200 && info.msg === 'token expirado o no valido') {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion");
            } else {
                setData(info.info);
                setFlautos(true);
            }
        });
    }

    const columns = [
        {
            name: 'Numero Factura',
            selector: row => row.numero,
        },
        {
            name: 'fecha',
            selector: row => row.fecha.toString(),
        },
        {
            name: 'estado',
            selector: row => row.estado,
        },
        {
            name: 'Metodo_Pago',
            selector: row => row.metodo_pago,
        },
        {
            name: 'Cantidad',
            selector: row => row.detallefactura[0].cantidad,
        },
        {
            name: 'Subtotal',
            selector: row =>   row.detallefactura[0].subtotal,
        },
        {
            name: 'Total',
            selector: row => row.total,
        },

    ];

    return (

        <div className='wrapper'>
            <Header />
            <div className="card text-center" style={{ margin: 20 }}>
                <div className="card-header" style={{ color: "black" }}>*** LISTA FACTURAS***</div>
                <div className="card-body">
                    <div className='container-fluid'>

                        <DataTable columns={columns} data={data} />
                    </div>
                </div>
            </div>
            <Footer />
        </div>


    );
}

export default ListarFactura;